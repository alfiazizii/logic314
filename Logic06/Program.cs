﻿using System;
using System.Collections.Generic;

namespace Logic06
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //ContohClass();
            //List();
            //ListClass();
            //ListAdd();
            ListRemove();

            Console.ReadKey();
        }

        static void ListRemove()
        {
            Console.WriteLine("=== List Remove ===");

            List<User> listofUsers = new List<User>
            {
                new User() { Name = "John Doe", Age = 20 },
                new User() { Name = "Jane Doe", Age = 21 },
                new User() { Name = "Joe Doe", Age = 22 }
            };

            listofUsers.Add(new User() { Name = "Alfi Azizi", Age = 23 });
            listofUsers.RemoveAt(2);

            for (int i = 0; i < listofUsers.Count; i++)
            {
                Console.WriteLine($"Nama User {i + 1} = {listofUsers[i].Name} sudah berumur {listofUsers[i].Age} tahun");
            }
        }

        static void ListAdd()
        {
            Console.WriteLine("=== List Add ===");

            List<User> listofUsers = new List<User>
            {
                new User() { Name = "John Doe", Age = 20 },
                new User() { Name = "Jane Doe", Age = 21 },
                new User() { Name = "Joe Doe", Age = 22 }
            };

            listofUsers.Add(new User() { Name = "Alfi Azizi", Age = 23});

            for (int i = 0; i < listofUsers.Count; i++)
            {
                Console.WriteLine($"Nama User {i + 1} = {listofUsers[i].Name} sudah berumur {listofUsers[i].Age} tahun");
            }
        }

        static void ListClass()
        {
            Console.WriteLine("=== List Class ===");

            List<User> listofUsers = new List<User>()
            {
                new User() { Name = "John Doe", Age = 20 },
                new User() { Name = "Jane Doe", Age = 21 },
                new User() { Name = "Joe Doe", Age = 22 }
            };

            for (int i = 0; i < listofUsers.Count; i++)
            {
                Console.WriteLine($"Nama User {i + 1} = {listofUsers[i].Name} sudah berumur {listofUsers[i].Age} tahun");
            }
        }

        static void ContohClass()
        {
            Mobil mobil = new Mobil("RI 1");

            mobil.percepat();
            mobil.maju();
            mobil.isiBensin(12);

            string platno = mobil.getPlatNo();

            Console.WriteLine($"Plat Nomor  : {platno}");
            Console.WriteLine($"Bensin      : {mobil.bensin}");
            Console.WriteLine($"Kecepatan   : {mobil.kecepatan}");
            Console.WriteLine($"Posisi      : {mobil.posisi}");
        }

        static void List()
        {
            Console.WriteLine("=== List ===");

            List<string> listofNames = new List<string>()
            {
                "John Doe",
                "Jane Doe",
                "Joe Doe"
            };

            Console.WriteLine(string.Join(", ", listofNames));
        }
    }
}