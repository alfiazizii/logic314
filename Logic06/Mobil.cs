﻿using System;

namespace Logic06
{
    public class Mobil
    {
        public double kecepatan;
        public double bensin;
        public double posisi;
        private string nama;
        private string platno;

        // constructor

        public Mobil(string _platno) 
        {
            platno = _platno;
        }

        // method
        public void percepat()
        {
            this.kecepatan += 10;
            this.bensin -= 5;
        }

        public void maju()
        {
            this.posisi += this.kecepatan;
            this.bensin -= 2;
        }

        public void isiBensin(double bensin)
        {
            this.bensin += bensin;
        }

        public string getPlatNo()
        {
            return platno;
        }
    }
}
