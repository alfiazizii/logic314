CREATE DATABASE DBPenerbit

-- 1. Buatlah Tabel Pengarang : tblPengarang		
CREATE TABLE tblPengarang(
ID int primary key identity(1,1),
Kd_Pengarang varchar (7) not null,
Nama varchar (30) not null,
Alamat varchar (80) not null,
Kota varchar (15) not null,
Kelamin varchar (1) not null
)

-- 1. Buatlah tabel Gaji : tblGaji			
CREATE TABLE tblGaji(
ID int primary key identity(1,1),
Kd_Pengarang varchar (7) not null,
Nama varchar (30) not null,
Gaji decimal (18,4) not null
)

-- 2. Masukan data dibawah kedalam table tblPengarang			
INSERT INTO tblPengarang(Kd_Pengarang, Nama, Alamat, Kota, Kelamin)
VALUES 
('P0001','Ashadi','Jl. Beo 25', 'Yogya', 'P'),
('P0002','Rian','Jl. Solo 123', 'Yogya', 'P'),
('P0003','Suwadi','Jl. Semangka 123', 'Bandung', 'P'),
('P0004','Siti','Jl. Durian 15', 'Solo', 'W'),
('P0005','Amir','Jl. Gajah 33', 'Kudus', 'P'),
('P0006','Suparman','Jl. Harimau 25', 'Jakarta', 'P'),
('P0007','Jaja','Jl. Singa 7', 'Bandung', 'P'),
('P0008','Saman','Jl. Naga 12', 'Yogya', 'P'),
('P0009','Anwar','Jl. Tidar 6A', 'Magelang', 'P'),
('P0010','Fatmawati','Jl. Renjana 4', 'Bogor', 'W')

-- 2. Masukan data dibawah kedalam table tblGaji			
INSERT INTO tblGaji(Kd_Pengarang, Nama, Gaji)
VALUES 
('P0002','Rian',600000),
('P0005','Amir',700000),
('P0004','Siti',500000),
('P0003','Suwadi',1000000),
('P0010','Fatmawati',600000),
('P0008','Saman',750000)

--1. Hitung dan tampilkan jumlah pengarang dari table tblPengarang.				
SELECT COUNT(ID) AS jumlahPengarang FROM tblPengarang

--2. Hitunglah berapa jumlah Pengarang Wanita dan Pria			
SELECT Kelamin, COUNT(ID) Jumlah FROM tblPengarang GROUP BY Kelamin

--3. Tampilkan record kota dan jumlah kotanya dari table tblPengarang.				
SELECT Kota, COUNT(Kota) AS JumlahKota FROM tblPengarang GROUP BY Kota

--4. Tampilkan record kota diatas 1 kota dari table tblPengarang.				
SELECT Kota, COUNT(Kota) AS [Kota > 1] FROM tblPengarang GROUP BY Kota
HAVING COUNT(Kota) > 1

--5. Tampilkan Kd_Pengarang yang terbesar dan terkecil dari table tblPengarang.
SELECT MIN(Kd_Pengarang) AS Terendah, MAX(Kd_Pengarang) AS Terbesar FROM tblPengarang
--Cara 2
SELECT
(SELECT TOP 1 Kd_Pengarang FROM tblPengarang ORDER BY Kd_Pengarang DESC) AS KODE_TERTINGGI,
(SELECT TOP 1 Kd_Pengarang FROM tblPengarang ORDER BY Kd_Pengarang ASC) AS KODE_TERENDAH

--6. Tampilkan gaji tertinggi dan terendah.		
SELECT MIN(Gaji) AS Gaji_Terendah, MAX(Gaji) AS Gaji_Terbesar FROM tblGaji

--7. Tampilkan gaji diatas 600.000.		
SELECT Gaji AS [Gaji > 600K] FROM tblGaji WHERE Gaji > 600000 

--8. Jumlah Gaji
SELECT SUM(Gaji) AS JumlahGaji FROM tblGaji

--9. Jumlah Gaji Berdasarkan Kota
SELECT Kota, SUM(Gaji) AS JumlahGaji FROM tblGaji g
JOIN tblPengarang p ON g.Kd_Pengarang = p.Kd_Pengarang 
GROUP BY p.Kota

--10. Tampilkan seluruh record pengarang antara P0003-P0006
SELECT * FROM tblPengarang
WHERE Kd_Pengarang BETWEEN 'P0003' AND 'P0006'

--11. Tampilkan seluruh data yogya, solo, dan magelang dari tabel pengarang.
SELECT * FROM tblPengarang WHERE Kota = 'Yogya' OR Kota = 'Solo' OR Kota = 'Magelang'
SELECT * FROM tblPengarang WHERE Kota IN('Yogya', 'Solo', 'Magelang')

--12. Tampilkan seluruh data yang bukan yogya dari tabel pengarang.
SELECT * FROM tblPengarang WHERE Kota NOT LIKE 'Yogya'
SELECT * FROM tblPengarang WHERE Kota != 'Yogya'
SELECT * FROM tblPengarang WHERE NOT Kota = 'Yogya'

--13. Tampilkan seluruh data pengarang yang dimulai dengan huruf [A]					
SELECT * FROM tblPengarang WHERE Nama LIKE 'a%'

--13. Tampilkan seluruh data pengarang yang berakhiran [i]
SELECT * FROM tblPengarang WHERE Nama LIKE '%i'

SELECT * FROM tblPengarang WHERE Kd_Pengarang NOT LIKE '%4' AND Nama LIKE '%i'

--13. Tampilkan seluruh data pengarang yang huruf ketiganya [a]	
SELECT * FROM tblPengarang WHERE Nama LIKE '__a%'

--13. Tampilkan seluruh data pengarang yang tidak berakhiran [n]	
SELECT * FROM tblPengarang WHERE Nama NOT LIKE '%n'

--14. Tampilkan seluruh data table tblPengarang dan tblGaji dengan Kd_Pengarang yang sama						 
SELECT p.ID, p.Kd_Pengarang, p.Nama, p.Alamat, p.Kota, p.Kelamin, g.Gaji 
FROM tblPengarang p
JOIN tblGaji g ON p.Kd_Pengarang = g.Kd_Pengarang

--15. Tampilan kota yang memiliki gaji dibawah 1.000.000			
SELECT p.Kota, g.Gaji FROM tblGaji g
JOIN tblPengarang p ON g.Kd_Pengarang = p.Kd_Pengarang
WHERE g.Gaji < 1000000

--16. Ubah panjang dari tipe kelamin menjadi 10
ALTER TABLE tblPengarang ALTER COLUMN kelamin varchar(10) NOT NULL

--17. Tambahkan kolom [Gelar] dengan tipe Varchar (12) pada tabel tblPengarang				
ALTER TABLE tblPengarang ADD Gelar varchar(12)

--18. Ubah alamat dan kota dari Rian di table tblPengarang menjadi, Jl. Cendrawasih 65 dan Pekanbaru						
UPDATE tblPengarang SET Alamat = 'Jl. Cenderawasih 65', Kota = 'Pekanbaru' WHERE Nama = 'Rian'

--19. Buatlah view untuk attribute Kd_Pengarang, Nama, Kota, Gaji dengan nama vwPengarang					
CREATE VIEW vwPengarang
AS
SELECT p.Kd_Pengarang,p.Nama,p.Kota,g.Gaji 
FROM tblPengarang p JOIN tblGaji g
ON p.Kd_Pengarang = g.Kd_Pengarang

-- Menampilkan Nomor Urut
SELECT *,ROW_NUMBER() OVER(ORDER BY nama) AS nomor_urut from tblPengarang