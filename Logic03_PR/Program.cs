﻿using System;

namespace Logic03_PR
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Logic03 - PR";
            //Tugas01();
            //Tugas02();
            //Tugas03();
            //Tugas04();
            //Tugas05();
            //Tugas06();
            //Tugas07();
            Tugas08();
            Console.ReadKey();
        }

        static void Tugas01()
        {
            Console.WriteLine(new string('=', 50));
            Console.WriteLine(new string(' ', 10) + "Soal 1 (Grade Nilai Mahasiswa)");
            Console.WriteLine(new string('=', 50));

            int nilai;

            Console.Write("Masukkan nilai anda = ");
            nilai = int.Parse(Console.ReadLine());

            if (nilai >= 90 && nilai < 100)
            {
                Console.WriteLine("Grade A");
            }
            else if (nilai >= 70 && nilai < 90)
            {
                Console.WriteLine("Grade B");
            }
            else if (nilai >= 50 && nilai < 70)
            {
                Console.WriteLine("Grade C");
            }
            else if (nilai >= 0 && nilai < 50)
            {
                Console.WriteLine("Grade E");
            }
            else
            {
                Console.WriteLine("Nilai Anda tidak valid.");
            }
        }

        static void Tugas02()
        {
            Console.WriteLine(new string('=', 50));
            Console.WriteLine(new string(' ', 13) + "Soal 2 (Pembelian Pulsa)");
            Console.WriteLine(new string('=', 50));
            int nominal, poin;

            Console.Write("Masukkan nominal pembelian pulsa Anda = ");
            nominal = int.Parse(Console.ReadLine());

            if (nominal >= 10000 && nominal < 25000)
            {
                poin = 80;
            }
            else if (nominal >= 25000 && nominal < 50000)
            {
                poin = 200;
            }
            else if (nominal >= 50000 && nominal < 100000)
            {
                poin = 400;
            }
            else if (nominal >= 100000)
            {
                poin = 800;
            }
            else
            {
                poin = 0;
            }
            Console.WriteLine(new string('=', 50));
            Console.WriteLine($"Pulsa Anda = {nominal}");
            Console.WriteLine($"Poin Anda = {poin}");
            Console.WriteLine(new string('=', 50));
        }

        static void Tugas03()
        {
            Console.WriteLine(new string('=', 40));
            Console.WriteLine(new string(' ', 8) + "Soal 3 (Diskon Grabfood)");
            Console.WriteLine(new string('=', 40));

            int belanja, jarak, ongkir = 5000;
            double diskon = 0, totalBelanja;
            string kodePromo;

            Console.Write("Nominal belanja Anda \t= ");
            belanja = int.Parse(Console.ReadLine());
            Console.Write("Jarak yang ditempuh \t= ");
            jarak = int.Parse(Console.ReadLine());
            Console.Write("Masukkan promo \t\t= ");
            kodePromo = Console.ReadLine().ToUpper();

            if (kodePromo == "JKTOVO")
            {
                if (belanja >= 30000)
                {
                    diskon = Math.Min(belanja * 0.4, 30000);
                }
            }

            if (jarak > 5)
            {
                ongkir += (jarak - 5) * 1000;
            }

            totalBelanja = (belanja - diskon) + ongkir;

            if (belanja > 0 && jarak > 0)
            {
                Console.WriteLine(new string('=', 40));
                Console.WriteLine(new string(' ', 13) + "Detail Belanja");
                Console.WriteLine(new string('=', 40));
                Console.WriteLine($"Belanja \t\t= {belanja.ToString("Rp #,##0")}");
                Console.WriteLine($"Diskon 40% \t\t= {diskon.ToString("Rp #,##0")}");
                Console.WriteLine($"Ongkir \t\t\t= {ongkir.ToString("Rp #,##0")}");
                Console.WriteLine($"Total Belanja \t\t= {totalBelanja.ToString("Rp #,##0")}");
                Console.WriteLine($"Promo yang digunakan \t= {kodePromo}");
                Console.WriteLine(new string('=', 40));
            }
            else
            {
                Console.WriteLine("Inputan nominal belanja / jarak tidak valid.");
            }
        }

        static void Tugas04()
        {
            Console.WriteLine(new string('=', 43));
            Console.WriteLine(new string(' ', 5) + "Soal 4 (Promo Free Ongkir Shopee)");
            Console.WriteLine(new string('=', 43));

            int belanja, ongkir, pilihanVoucher, diskonBelanja = 0, diskonOngkir = 0, totalBelanja;

            Console.Write("Nominal belanja Anda \t= ");
            belanja = int.Parse(Console.ReadLine());
            Console.Write("Ongkos kirim Anda \t= ");
            ongkir = int.Parse(Console.ReadLine());
            ulang:
            Console.Write("Pilih voucher [1/2/3] \t= ");
            pilihanVoucher = int.Parse(Console.ReadLine());

            if (pilihanVoucher == 1 && belanja >= 30000)
            {
                diskonOngkir = 5000;
                diskonBelanja = 5000;
            }
            else if (pilihanVoucher == 2 && belanja >= 50000)
            {
                diskonOngkir = 10000;
                diskonBelanja = 10000;
 
            }
            else if (pilihanVoucher == 3 && belanja >= 100000)
            {
                
                diskonOngkir = 20000;
                diskonBelanja = 10000;
                
            }

            totalBelanja = (belanja - diskonBelanja - diskonOngkir) + ongkir;

            if (belanja > 0 && ongkir > 0)
            {
                Console.WriteLine(new string('=', 43));
                Console.WriteLine(new string(' ', 15) + "Detail Belanja");
                Console.WriteLine(new string('=', 43));
                Console.WriteLine($"Belanja \t= {belanja.ToString("Rp #,##0")}");
                Console.WriteLine($"Ongkos kirim \t= {ongkir.ToString("Rp #,##0")}");
                Console.WriteLine($"Diskon Ongkir \t= {diskonOngkir.ToString("Rp #,##0")}");
                Console.WriteLine($"Diskon Belanja \t= {diskonBelanja.ToString("Rp #,##0")}");
                Console.WriteLine($"Total Belanja \t= {totalBelanja.ToString("Rp #,##0")}");
                Console.WriteLine(new string('=', 43));
            }
            else
            {
                Console.WriteLine("Inputan tidak valid.");
            }

        }

        static void Tugas05()
        {
            Console.WriteLine(new string('=', 45));
            Console.WriteLine(new string(' ', 10) + "Soal 5 (Istilah Generasi)");
            Console.WriteLine(new string('=', 45));

            string nama;
            int tahunLahir;

            Console.Write("Masukkan nama Anda \t= ");
            nama = Console.ReadLine();
            Console.Write("Tahun berapa Anda lahir ? ");
            tahunLahir = int.Parse(Console.ReadLine());

            if (tahunLahir >= 1995)
            {
                Console.WriteLine($"{nama}, Anda termasuk generasi Z");
            }
            else if (tahunLahir >= 1980)
            {
                Console.WriteLine($"{nama}, Anda termasuk generasi Y");
            }
            else if (tahunLahir >= 1965)
            {
                Console.WriteLine($"{nama}, Anda termasuk generasi X");
            }
            else if (tahunLahir >= 1944)
            {
                Console.WriteLine($"{nama}, Anda termasuk generasi Baby Boomer");
            }
            else
            {
                Console.WriteLine("Inputan Anda tidak valid.");
            }
        }

        static void Tugas06()
        {
            Console.WriteLine(new string('=', 60));
            Console.WriteLine(new string(' ', 18) + "Soal 6 (Gaji Karyawan)");
            Console.WriteLine(new string('=', 60));

            double pajak = 0, tunjangan, gaji, gapok, bpjs, totalGaji;
            int banyakBulan;
            string nama;

            Console.Write("Masukkan nama Anda \t\t= ");
            nama = Console.ReadLine();
            Console.Write("Masukkan tunjangan Anda \t= ");
            tunjangan = double.Parse(Console.ReadLine());
            Console.Write("Masukkan gaji pokok Anda \t= ");
            gapok = double.Parse(Console.ReadLine());

            if (gapok + tunjangan <= 5000000)
            {
                pajak = 0.05;
            }
            else if (gapok + tunjangan > 5000000 && gapok + tunjangan <= 10000000)
            {
                pajak = 0.10;
            }
            else if (gapok + tunjangan > 10000000)
            {
                pajak = 0.15;
            }


            Console.Write("Masukkan banyak bulan \t\t= ");
            banyakBulan = int.Parse(Console.ReadLine());

            pajak = pajak * (gapok + tunjangan);
            bpjs = 0.03 * (gapok + tunjangan);
            gaji = (gapok + tunjangan) - (pajak + bpjs);
            totalGaji = gaji * banyakBulan;

            if (tunjangan >= 0 && totalGaji >= 0 && banyakBulan >= 0)
            {
                Console.WriteLine(new string('=', 60));
                Console.WriteLine($"Karyawan atas nama {nama} slip gaji sebagai berikut : ");
                Console.WriteLine($"Pajak \t\t\t\t= {pajak.ToString("Rp #,##0.00")}");
                Console.WriteLine($"BPJS \t\t\t\t= {bpjs.ToString("Rp #,##0.00")}");
                Console.WriteLine($"Gaji / Bulan \t\t\t= {gaji.ToString("Rp #,##0.00")}");
                Console.WriteLine($"Total gaji / banyak bulan \t= {totalGaji.ToString("Rp #,##0.00")}");
            }
            else
            {
                Console.WriteLine("Inputan tidak valid");
            }


        }

        static void Tugas07()
        {
            double beratBadan, tinggiBadan, bmi;
            string status;

            Console.WriteLine(new string('=', 50));
            Console.WriteLine(new string(' ', 13) + "Soal 7 (Perhitungan BMI)");
            Console.WriteLine(new string('=',
                50));
            Console.Write("Masukkan berat banda Anda (kg) : ");
            beratBadan = double.Parse(Console.ReadLine());
            Console.Write("Masukkan tinggi badan Anda (cm) : ");
            tinggiBadan = double.Parse(Console.ReadLine());

            bmi = beratBadan / Math.Pow((tinggiBadan / 100), 2);

            if (bmi < 18.5)
            {
                status = "kurus";
            }
            else if (bmi >= 18.5 && bmi < 25)
            {
                status = "langsing";
            }
            else if (bmi >= 25 && bmi < 30)
            {
                status = "gemuk";
            }
            else
            {
                status = "obesitas";
            }

            if(tinggiBadan > 0 && beratBadan > 0)
            {
                Console.WriteLine(new string('=', 50));
                Console.WriteLine("Nilai BMI Anda adalah : " + Math.Round(bmi, 4));
                Console.WriteLine($"Anda termasuk berbadan {status}");
                Console.WriteLine(new string('=', 50));
            }
            else
            {
                Console.WriteLine("Inputan tidak valid.");
            }
        }

        static void Tugas08()
        {
            double mtk, fisika, kimia, ratarata;

            Console.Write("Masukkan nilai MTK : ");
            mtk = double.Parse(Console.ReadLine());
            Console.Write("Masukkan nilai Fisika : ");
            fisika = double.Parse(Console.ReadLine());
            Console.Write("Masukkan nilai Kimia : ");
            kimia = double.Parse(Console.ReadLine());

            ratarata = (mtk + fisika + kimia) / 3;

            Console.WriteLine("Nilai Rata-Rata : " + ratarata);

            if (ratarata >= 50 && ratarata <= 100)
            {
                Console.WriteLine("Selamat\nKamu Berhasil\nKamu Hebat");
            }
            else if (ratarata > 0 && ratarata < 50)
            {
                Console.WriteLine("Maaf\nKamu Gagal");
            }
            else
            {
                Console.WriteLine("Nilai invalid");
            }
        }
    }
}
