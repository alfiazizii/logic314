﻿using System;

namespace Logic04
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //RemoveString();
            //InsertString();
            ReplaceString();

            Console.ReadKey();
        }

        static void RemoveString()
        {
            Console.WriteLine("=== Remove String ===");
            Console.Write("Masukkan kalimat : ");
            string kalimat = Console.ReadLine();
            Console.Write("Isi parameter Remove : ");
            int param = int.Parse(Console.ReadLine());

            Console.WriteLine($"Hasil Remove String : {kalimat.Remove(param)}");
        }

        static void InsertString()
        {
            Console.WriteLine("=== Insert String ===");
            Console.Write("Masukkan kalimat : ");
            string kalimat = Console.ReadLine();
            Console.Write("Isi parameter Insert : ");
            int param = int.Parse(Console.ReadLine());
            Console.Write("Masukkan input string : ");
            string input = Console.ReadLine();  

            Console.WriteLine($"Hasil Insert String : {kalimat.Insert(param, input)}");
        }

        static void ReplaceString()
        {
            Console.WriteLine("=== Replace String ===");
            Console.Write("Masukkan kalimat : ");
            string kalimat = Console.ReadLine();
            Console.Write("Masukkan kata lama yang ingin di Replace: ");
            string kataLama = Console.ReadLine();
            Console.Write("Masukkan kata baru yang ingin di Replace: ");
            string kataBaru = Console.ReadLine();

            Console.WriteLine($"Hasil Remove String : {kalimat.Replace(kataLama, kataBaru)}");
        }
    }
}
