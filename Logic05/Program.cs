﻿using System;

namespace Logic05
{
    internal class Program
    {
        static void Main(string[] args)
        {
            ConvertArrayAll();
            //PadLeft();
            //Contain();
            Console.ReadKey();
        }

        static void ConvertArrayAll()
        {
            Console.Write("Masukkan angka array (paka koma) = ");
            int[] arr = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);

            Console.WriteLine(string.Join(" ", arr));
        }

        static void PadLeft()
        {
            Console.Write("Masukkan kalimat = ");
            string kalimat = Console.ReadLine();
            Console.Write("Masukkan panjang = ");
            int panjang = int.Parse(Console.ReadLine());
            Console.Write("Masukkan Karakter = ");
            char karakter = char.Parse(Console.ReadLine()); 

            Console.WriteLine($"Hasil PadLeft : {kalimat.ToString().PadLeft(panjang, karakter)}");
        }

        static void Contain()
        {
            Console.Write("Masukkan kalimat = ");
            string kalimat = Console.ReadLine();
            Console.Write("Masukkan contain : ");
            string contain = Console.ReadLine();

            if (kalimat.Contains(contain))
            {
                Console.WriteLine($"Kalimat ({kalimat}) ini mengandung mengandung {contain}");
            }
            else
            {
                Console.WriteLine($"Kalimat ({kalimat}) ini mengandung tidak mengandung {contain}");
            }
        }
    }
}
