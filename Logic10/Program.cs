﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Logic10
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //SimpleArraySum();
            //MiniMaxSum();
            arrayy();
            Console.ReadKey();
        }

        static void arrayy()
        {
            int[] arr = new int[6];
            arr[0] = -4;
            arr[1] = 3;
            arr[2] = -9;
            arr[3] = 0;
            arr[4] = 4;
            arr[5] = 1;

            double nol = 0;
            double positif = 0;
            double negatif = 0;

            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] == 0)
                {
                    nol++;
                }
                else if (arr[i] > 0)
                {
                    positif++;
                }
                else
                {
                    negatif++;
                }
            }

            double hasilPositif = positif /= arr.Length;
            double hasilNegatif = negatif /= arr.Length;
            double hasilNol = nol /= arr.Length;

            Console.WriteLine(hasilPositif);
            Console.WriteLine(hasilNegatif);
            Console.WriteLine(hasilNol);
        }

        static void SimpleArraySum()
        {
            Console.WriteLine("=== Simple Array Sum ===");

            List<int> list = new List<int>();

            list.Add(1);
            list.Add(2);
            list.Add(3);
            list.Add(4);
            list.Add(10);
            list.Add(11);

            // cara kedua tambah index list. Tidak bisa dipakai untuk looping
            List<int> list2 = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9 };

            int hasil = SimpleArraySum(list);

            Console.WriteLine(hasil);
        }

        static int SimpleArraySum(List<int> ar)
        {
            int result = 0;

            for(int i = 0; i < ar.Count; i++)
            {
                result+= ar[i];
            }

            return result;
        }

        static void MiniMaxSum()
        {
            Console.WriteLine("=== Simple Array Sum ===");

            List<int> list = new List<int>();

            list.Add(1);
            list.Add(3);
            list.Add(5);
            list.Add(7);
            list.Add(9);

            int hasil = MiniMaxSum(list);

            Console.WriteLine(hasil);
        }

        static int MiniMaxSum(List<int> arr)
        {
            int hasilMin = 0;
            int hasilMaks = 0;
            int min = arr.Min();
            int maks = arr.Max();

            for (int i = 0; i < arr.Count; i++)
            {
               hasilMin += arr[i];
               hasilMaks += arr[i];
            }

            hasilMin = hasilMin - maks;
            hasilMaks = hasilMaks - min;

            return hasilMaks;
        }
    }
}
