﻿using System;
using System.Drawing;

namespace Logic03
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Tugas Hari ke 3";
            //Soal01();
            //Soal02();
            //Soal03();
            //Soal04();
            //PerulanganWhile();
            //PerulanganDoWhile();
            //PerulanganForIncrement();
            //PerulanganForDecrement();
            //Break();
            //Continue();
            //ForBersarang();
            //ArrayStatic();
            //ArrayForEach();
            //ArrayFor();
            //Array2Dimensi();
            //Array2DimensiFor();
            //SplitJoinString();
            //Substring();
            //StringToCharArray();
            Console.ReadKey();
        }

        static void StringToCharArray()
        {
            Console.Write("Masukkan kalimat : ");
            string kalimat = Console.ReadLine();
            char[] charArr = kalimat.ToCharArray();

            foreach (char ch in charArr)
            {
                Console.WriteLine(ch);
            }
        }

        static void Substring()
        {
            Console.Write("Masukkan kalimat : ");
            string kalimat = Console.ReadLine();

            Console.WriteLine("Substring(1, 4) : " + kalimat.Substring(1, 4));
            Console.WriteLine("Substring(5, 2) : " + kalimat.Substring(5, 2));
            Console.WriteLine("Substring(7, 9) : " + kalimat.Substring(7, 9));
            Console.WriteLine("Substring(9) : " + kalimat.Substring(9));
        }

        static void SplitJoinString()
        {
            Console.Write("Masukkan kalimat = ");
            string kalimat = Console.ReadLine();
            string[] kataArray = kalimat.Split(" ");

            foreach (string kata in kataArray)
            {
                Console.WriteLine(kata);
            }

            Console.WriteLine(string.Join(",", kataArray));
        }

        static void Array2DimensiFor()
        {
            int[,] array = new int[,]
            {
                {1,2,3},
                {4,5,6},
                {7,8,9}
            };

            for (int k = 0; k < 3; k++)
            {
                for (int j = 0; j < 3; j++)
                {
                    Console.Write(array[k, j] + " ");
                }
                Console.WriteLine();
            }
        }

        static void Array2Dimensi()
        {
            int[,] array = new int[,]
            {
                {1,2,3},
                {4,5,6},
                {7,8,9}
            };

            Console.WriteLine(array[0, 1]);
            Console.WriteLine(array[0, 2]);
            Console.WriteLine(array[1, 1]);
            Console.WriteLine(array[1, 2]);
        }

        static void ArrayFor()
        {
            string[] array = new string[]
            {
                "Abdullah Muafa",
                "Isni Dwitiniardi",
                "Ilham Rizki",
                "Marchelino",
                "Alwi Fadli Siregar"
            };

            for (int i = 1; i < array.Length; i++)
            {
                Console.WriteLine(array[i]);
            }
        }

        static void ArrayForEach()
        {
            string[] array = new string[]
            {
                "Abdullah Muafa",
                "Isni Dwitiniardi",
                "Ilham Rizki",
                "Marchelino",
                "Alwi Fadli Siregar"
            };

            foreach (string item in array)
            {
                Console.WriteLine(item);
            }
        }

        static void ArrayStatic()
        {
            int[] staticInArray = new int[3];

            //isi array
            staticInArray[0] = 1;
            staticInArray[1] = 2;
            staticInArray[2] = 3;

            //cetak array
            Console.WriteLine(staticInArray[0]);
            Console.WriteLine(staticInArray[1]);
            Console.WriteLine(staticInArray[2]);
        }

        static void ForBersarang()
        {
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    Console.Write($"[{i},{j}]");
                }
                Console.WriteLine();
            }
        }
 
        static void Continue()
        {
            for (int i = 0; i < 10; i++)
            {
                if (i == 5)
                {
                    continue;
                }
                Console.WriteLine(i);
            }
        }

        static void Break()
        {
            for (int i = 0; i < 10; i++)
            {
                if (i == 5)
                {
                    break;
                }
                Console.WriteLine(i);
            }
        }

        static void PerulanganForDecrement()
        {
            for (int i = 10; i > 0; i--)
            {
                Console.WriteLine(i);
            }
        }

        static void PerulanganForIncrement()
        {
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine(i);
            }
        }

        static void PerulanganDoWhile()
        {
            int a = 0;
            do
            {
                Console.WriteLine(a);
                a++;
            } while (a < 5);
        }

        static void PerulanganWhile()
        {
            Console.WriteLine("=== Perulangan While ===");
            bool ulangi = true;
            int nilai = 1;

            while(ulangi)
            {
                Console.WriteLine($"Proses ke : {nilai}");
                nilai++;

                Console.Write("Ulangi proses? [y/n] : ");
                string input = Console.ReadLine().ToLower();

                if(input == "y")
                {
                    ulangi = true;
                } 
                else if(input == "n")
                {
                    ulangi = false;
                }
                else
                {
                    nilai = 1;
                    Console.WriteLine("Input yang anda masukkan tidak valid");
                    ulangi = true;
                }
            } 
        }

        static void Soal01()
        {
            Console.WriteLine("=== Soal Modulus ===");
            int angka, pembagi;
           
            ulang:
            Console.Write("Masukkan nilai = ");
            angka = int.Parse(Console.ReadLine());
            Console.Write("Masukkan pembagi = ");
            pembagi = int.Parse(Console.ReadLine());

            if(angka % pembagi == 0 )
            {
                Console.WriteLine($"Angka {angka} % {pembagi} adalah 0");
            } else
            {
                Console.WriteLine($"Angka {angka} % {pembagi} bukan 0 melainkan hasil mod");
            }
            goto ulang;
        }

        static void Soal02()
        {
            Console.WriteLine("=== Soal Puntung Rokok ===");
            int puntungRokok, rokok, sisa, harga;

            ulang:
            Console.Write("Masukkan jumlah puntung rokok = ");
            puntungRokok = int.Parse(Console.ReadLine());

            rokok = puntungRokok / 8;
            sisa = puntungRokok % 8;
            harga = rokok * 500;

            if(puntungRokok >= 1)
            {
                Console.WriteLine($"Jumlah puntung rokok adalah {puntungRokok} menghasilkan {rokok} batang rokok dengan sisa {sisa} puntung rokok");
                Console.WriteLine($"Hasil penjualan dari {rokok} batang rokok adalah {harga} rupiah");
            }
            else
            {
                Console.WriteLine("Kumpulin puntung rokok dulu!");
            }

            goto ulang;
        }

        static void Soal03()
        {
            Console.WriteLine("=== Soal Grade Nilai ===");
            int nilai;

            ulang:
            Console.Write("Masukkan nilai = ");
            nilai = int.Parse(Console.ReadLine());

            if(nilai >= 80 && nilai <= 100)
            {
                Console.WriteLine("Anda mendapatkan grade A");
            }
            else if(nilai >= 60 && nilai < 80)
            {
                Console.WriteLine("Anda mendapatkan grade B");
            }
            else if(nilai >= 1 && nilai < 60)
            {
                Console.WriteLine("Anda mendapatkan grade C");
            }
            else
            {
                Console.WriteLine("Nilai tidak valid");
            }
            goto ulang;
        }

        static void Soal04()
        {
            Console.WriteLine("=== Soal Ganjil Genap ===");
            int angka;

            ulang:
            Console.Write("Masukkan angka = ");
            angka = int.Parse(Console.ReadLine());

            if(angka % 2 == 0)
            {
                Console.WriteLine($"Angka {angka} adalah bilangan genap");
            }
            else
            {
                Console.WriteLine($"Angka {angka} adalah bilangan ganjil");
            }
            goto ulang;
        }
    }
}
