﻿using System;

namespace Logic04_PR
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //Tugas01();
            //Tugas02();
            //Tugas03();
            //Tugas04();
            //Tugas05();
            //Tugas06();
            //Tugas07();
            //Tugas08();
            //Tugas09();
            //Tugas10();
            //Tugas11();
            //Tugas12();
            Tugas12Ver2();
            Console.ReadKey();
        }

        static void Tugas01()
        {
            Console.WriteLine(new string('=', 40));
            Console.WriteLine(new string(' ', 6) + "Soal 1 (Penggajian Karyawan)");
            Console.WriteLine(new string('=', 40));

            int golongan, jamKerja, jamNormal = 0, jamLembur = 0;
            double upahPerJam = 0, gajiMingguan = 0, upahLembur = 0, upahMingguan = 0;

            Console.Write("Masukkan golongan karyawan (1-4): ");
            golongan = int.Parse(Console.ReadLine());
            Console.Write("Masukkan jumlah jam kerja per minggu: ");
            jamKerja = int.Parse(Console.ReadLine());


            if (golongan == 1)
            {
                upahPerJam = 2000;
            }
            else if (golongan == 2)
            {
                upahPerJam = 3000;
            }
            else if (golongan == 3)
            {
                upahPerJam = 4000;
            }
            else if (golongan == 4)
            {
                upahPerJam = 5000;
            }
            else
            {
                Console.WriteLine("Golongan karyawan tidak valid.");
            }

            if (jamKerja > 40)
            {
                jamNormal = 40;
                upahMingguan = jamNormal * upahPerJam;
                jamLembur = jamKerja - jamNormal;
                upahLembur = jamLembur * upahPerJam * 1.5;
                gajiMingguan = upahMingguan + upahLembur;
            }
            else
            {
                upahMingguan = jamKerja * upahPerJam;
                gajiMingguan = upahMingguan;

            }
            Console.WriteLine("Upah karyawan adalah: " + upahMingguan);
            Console.WriteLine("Upah lembur karyawan adalah: " + upahLembur);
            Console.WriteLine("Gaji mingguan karyawan adalah: " + gajiMingguan);
        }

        static void Tugas02()
        {

            Console.Write("Masukkan kalimat: ");
            string[] kalimat = Console.ReadLine().Split(" ");
           
            for (int i = 0; i < kalimat.Length; i++)
            {
                Console.WriteLine($"Kata {i + 1} {kalimat[i]}");
            }

            Console.WriteLine("Total kata adalah " + kalimat.Length);
        }

        static void Tugas03()
        {
            Console.Write("Masukkan kalimat = ");
            string kalimat = Console.ReadLine();

            string[] kata = kalimat.Split(' ');

            for (int i = 0; i < kata.Length; i++)
            {

                for (int j = 0; j < kata[i].Length; j++)
                {
                    if (j == 0)
                    {
                        Console.Write(kata[i][j]);
                    }
                    else if (j == kata[i].Length - 1)
                    {
                        Console.Write(kata[i][j]);
                    }
                    else
                    {
                        //Console.Write("*");
                        Console.Write(kata[i].Replace(kata[i], "*"));
                    }
                }
                Console.Write(" ");
            }
        }

        static void Tugas04()
        {
            Console.Write("Masukkan kalimat = ");
            string[] kalimat = Console.ReadLine().Split(" ");

            for (int i = 0; i < kalimat.Length; i++)
            {
                for (int j = 0; j < kalimat[i].Length; j++)
                {
                    if (j == 0)
                    {
                        Console.Write("*");
                    }
                    else if (j == kalimat[i].Length - 1)
                    {
                        Console.Write("*");
                    }
                    else
                    {
                        Console.Write(kalimat[i][j]);
                    }
                }
                Console.Write(" ");
            }
        }

        static void Tugas05()
        {
            Console.Write("Masukkan kalimat = ");
            string[] kalimat = Console.ReadLine().Split(" ");

            int totalKata = kalimat.Length;

            for (int i = 0; i < totalKata; i++)
            {
                for (int j = 0; j < kalimat[i].Length; j++)
                {
                    if(j == 0)
                    {
                        Console.Write(kalimat[i].ToString().Remove(j));
                        Console.Write(' ');
                    }
                    else
                    {
                        Console.Write(kalimat[i][j]);
                    }
                }
            }
        }

        static void Tugas06()
        {
            Console.Write("Masukkan N = ");
            int n = int.Parse(Console.ReadLine());

            int[] arr;
            arr = new int[n];
            int sum = 1;

            for (int i = 0; i < arr.Length; i++)
            {
                sum *= 3;
                arr[i] = sum;
                if (i % 2 == 0)
                {
                    Console.Write($"{arr[i]} ");
                }
                else
                {
                    Console.Write("* ");
                }
            }
        }

        static void Tugas07()
        {
            Console.Write("Masukkan N = ");
            int n = int.Parse(Console.ReadLine());

            int[] arr;
            arr = new int[n];
            int sum = 5;

            for (int i = 0; i < arr.Length; i++)
            {
                int nilai = i % 2;
                if (nilai == 0)
                {
                    int total = -sum * (i + 1);
                    arr[i] = total;
                    Console.Write($"{arr[i]} ");
                }
                else
                {
                    int total = sum * (i + 1);
                    arr[i] = total;
                    Console.Write($" {arr[i]} ");
                }
            }
        }

        static void Tugas08()
        {
            Console.Write("Masukkan Bilangan : ");
            int n = int.Parse(Console.ReadLine());

            int n1 = 0, n2 = 1, n3;

            for (int i = 0; i < n; i++)
            {
                n3 = n1 + n2;
                n1 = n2;
                n2 = n3;

                Console.Write($"{n1} ");
            }
        }

        static void Tugas08_2()
        {
            Console.WriteLine("=== Fibonnaci 2 ===");
            Console.Write("Masukkan angka = ");
            int number = int.Parse(Console.ReadLine());

            int[] numberArray = new int[number];

            for (int i = 0; i < number; i++)
            {
                if(i <= 1)
                {
                    numberArray[i] = 1;
                }
                else
                {
                    numberArray[i] = numberArray[i - 1] + numberArray[i - 1];
                }
            }
            Console.Write(string.Join(" ", numberArray));
        }

        static void Tugas09()
        {
            Console.WriteLine("=== Konversi Waktu ===");
            Console.Write("Masukkan format waktu = ");
            string formatJam = Console.ReadLine().ToUpper();

            string pm = formatJam.Substring(8, 2);
            int jam = int.Parse(formatJam.Substring(0, 2));

            if (jam <= 12)
            {
                if (pm == "PM")
                {
                    jam += 12;
                    Console.WriteLine(jam.ToString() + formatJam.Substring(2, 6));
                }
                else
                {
                    Console.WriteLine(formatJam.Substring(0, 8));
                }
            }
            else
            {
                Console.WriteLine("Input yang bener!");
            }
        }

        static void Tugas10()
        {
            string merkBaju;
            int hargaBaju = 0;
            string kodeUkuran = "";

            ulang:
            Console.Write("Masukkan Kode Baju = ");
            int kodeBaju = int.Parse(Console.ReadLine());
            Console.Write("Masukkan Kode Ukuran (S, M, L, XL, XXL)= ");
            kodeUkuran = Console.ReadLine().ToLower();

            if (kodeBaju == 1)
            {
                merkBaju = "IMP";

                if (kodeUkuran == "s")
                {
                    hargaBaju = 200000;
                }
                else if (kodeUkuran == "m")
                {
                    hargaBaju = 220000;
                }
                else if (kodeUkuran == "l" || kodeUkuran == "xl")
                {
                    hargaBaju = 250000;
                }
                else
                {
                    Console.WriteLine("Inputan anda tidak valid.");
                }

            }
            else if (kodeBaju == 2)
            {
                merkBaju = "Prada";

                if (kodeUkuran == "s")
                {
                    hargaBaju = 150000;
                }
                else if (kodeUkuran == "m")
                {
                    hargaBaju = 160000;
                }
                else
                {
                    hargaBaju = 170000;
                }
            }
            else if (kodeBaju == 3)
            {
                merkBaju = "Gucci";

                if (kodeUkuran == "s" || kodeUkuran == "m" || kodeUkuran == "l" || kodeUkuran == "xl")
                {
                    hargaBaju = 200000;
                }
                else
                {
                    Console.WriteLine("Inputan anda tidak valid.");
                }
            }
            else
            {
                Console.WriteLine("Inputan tidak valid");
                goto ulang;
            }

            Console.WriteLine($"Merk Baju : {merkBaju}");
            Console.WriteLine($"Harga Baju : {hargaBaju}");
        }

        static void Tugas11()
        {
            Console.Write("Masukkan Uang Andi : ");
            int uang = int.Parse(Console.ReadLine());
            Console.Write("Masukkan harga baju (pakai koma) : ");
            int[] hargaBajuArray = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);
            Console.Write("Masukkan Harga Celana (pakai koma) : ");
            int[] hargaCelanaArray = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);

            int maxBelanja = 0;
            for (int i = 0; i < hargaBajuArray.Length; i++)
            {
                for (int j = 0; j < hargaCelanaArray.Length; j++)
                {
                    int harga = hargaBajuArray[i] + hargaCelanaArray[j];
                    if(harga <= uang && harga >= maxBelanja)
                    {
                        maxBelanja = harga;
                    }
                }
            }
            Console.WriteLine(maxBelanja);
        }

        static void Tugas12()
        {

            Console.Write("Masukkan angka: ");
            int angka = int.Parse(Console.ReadLine());
            Console.WriteLine();

            int[] arr = new int[angka];

            string tampung = "";

            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = i + 1;
            }

            for (int j = 0; j < arr.Length; j++)
            {
                for (int k = 0; k < arr.Length; k++)
                {
                    if(j == 0)
                    {
                        tampung += arr[k] + "\t";
                        //Console.Write($"{arr[k]} ");
                    }
                    else if(j == arr.Length - 1)
                    {
                        tampung += arr[(arr.Length - 1) - k] + "\t";
                        //Console.Write($"{arr[arr.Length - 1 - k]} ");
                    }
                    else
                    {
                        if (k == 0)
                        {
                            tampung += "*\t";
                        }
                        else if (k == arr.Length - 1)
                        {
                            tampung += "*\t";
                        }
                        else
                        {
                            tampung += "\t";
                        }
                    }
                }
                tampung += "\n";
            }
            Console.WriteLine(tampung);
        }

        static void Tugas12Ver2()
        {
            Console.Write("Masukkan inputan (angka) = ");
            int n = int.Parse(Console.ReadLine());

            for (int baris = 1; baris <= n; baris++)
            {
                for (int kolom = 1; kolom <= n; kolom++)
                {
                    if (baris == 1 || baris == n)
                    {
                        if (baris == n)
                        {
                            Console.Write(n - kolom + 1 + "\t");
                        }
                        else
                        {
                            Console.Write(kolom + "\t");
                        }
                    }
                    else
                    {
                        if(kolom == 1 || kolom == n)
                        {
                            Console.Write("*\t");
                        }
                        else
                        {
                            Console.Write(" \t");
                        }
                    }
                }
                Console.WriteLine();
            }
        }
    }
}
