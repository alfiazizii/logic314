﻿// library
using System;

// namespace adalah suatu nama yang menampung banyak class
namespace logic314
{
    // identifier or modifier
    internal class Program
    {
        // static tidak dapat berubah
        // void tidak menghasilkan return
        // Main adalah sebuah fungsi yang akan dijalankan saat program dijalankan
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Console.WriteLine("Writeline menghasilkan baris baru");
            Console.Write("Write tidak menghasilkan baris baru");
            Console.WriteLine("Hello Gaes!");
            Console.Write("Masukkan nama anda : ");
            string nama = Console.ReadLine();

            // 3 cara pemanggilan variabel
            Console.WriteLine("Nama saya adalah : " + nama);
            Console.WriteLine("Nama saya adalah : {0}", nama);
            Console.WriteLine($"Nama saya adalah : {nama}");
            Console.ReadKey();
        }
    }
}
