﻿using System;
using System.Reflection;

namespace Logic01
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //OperatorAritmatika();
            //OperatorModulus();
            //OperatorPenugasan();
            //OperatorPerbandingan();
            //OperatorLogika();
            Penjumlahan();
            //Konversi();

            Console.ReadKey();
        }

        static void Penjumlahan()
        {
            int mangga, apel;

            Console.WriteLine("=== Method ReturnType ===");
            Console.Write("Jumlah mangga = ");
            mangga = Convert.ToInt32(Console.ReadLine());
            Console.Write("Jumlah apel = ");
            apel = Convert.ToInt32(Console.ReadLine());

            int hasil = HitungJumlah(mangga, apel);
            Console.WriteLine($"Hasil mangga + apel = {hasil}");
        }

        static int HitungJumlah(int mangga, int apel)
        {
            int hasil = 0;
            hasil = mangga + apel;
            return hasil;
        }

        static void OperatorAritmatika()
        {
            Console.WriteLine("=== Penjumlahan ===");
            int mangga, apel, hasil = 0;
            Console.Write("Jumlah mangga : ");
            mangga = int.Parse(Console.ReadLine());
            Console.Write("Jumlah apel : ");
            apel = int.Parse(Console.ReadLine());

            hasil = mangga + apel;
            Console.WriteLine($"Hasil mangga + apel = {hasil}");

            Console.WriteLine("\n=== Pengurangan ===");
            Console.Write("Jumlah mangga : ");
            mangga = int.Parse(Console.ReadLine());
            Console.Write("Jumlah apel : ");
            apel = int.Parse(Console.ReadLine());

            hasil = mangga - apel;
            Console.WriteLine($"Hasil mangga - apel = {hasil}");

            Console.WriteLine("\n=== Perkalian ===");
            Console.Write("Jumlah mangga : ");
            mangga = int.Parse(Console.ReadLine());
            Console.Write("Jumlah apel : ");
            apel = int.Parse(Console.ReadLine());

            hasil = mangga * apel;
            Console.WriteLine($"Hasil mangga * apel = {hasil}");

            Console.WriteLine("\n=== Pembagian ===");
            Console.Write("Jumlah mangga : ");
            mangga = int.Parse(Console.ReadLine());
            Console.Write("Jumlah apel : ");
            apel = int.Parse(Console.ReadLine());

            hasil = mangga / apel;
            Console.WriteLine($"Hasil mangga / apel = {hasil}");
        }

        static void OperatorModulus()
        {
            int mangga, apel, hasil = 0;

            Console.WriteLine("=== Modulus ===");
            Console.Write("Jumlah mangga : ");
            mangga = int.Parse(Console.ReadLine());
            Console.Write("Jumlah apel : ");
            apel = int.Parse(Console.ReadLine());

            hasil = mangga % apel;
            Console.WriteLine($"Hasil mangga % apel = {hasil}");
        }

        static void OperatorPenugasan()
        {
            int mangga = 10;
            int apel = 8;

            mangga = 15;
            Console.WriteLine($"Nilai mangga = {mangga}");
            Console.WriteLine($"Nilai apel = {apel}");
            apel = 8;
            mangga = 15;
            apel += mangga;
            Console.WriteLine($"Penambahan dan penugasan = {apel}");
            apel = 8;
            mangga = 15;
            apel -= mangga;
            Console.WriteLine($"Pengurangan dan penugasan = {apel}");
            apel = 8;
            mangga = 15;
            apel *= mangga;
            Console.WriteLine($"Perkalian dan penugasan = {apel}");
            apel = 8;
            mangga = 15;
            apel /= mangga;
            Console.WriteLine($"Pembagian dan penugasan = {apel}");
            apel = 8;
            mangga = 15;
            apel %= mangga;
            Console.WriteLine($"Modulus dan penugasan = {apel}");
        }

        static void OperatorPerbandingan()
        {
            int mangga, apel = 0;
            
            Console.WriteLine("=== Operator Perbandingan ===");
            Console.Write("Jumlah mangga = ");
            mangga = int.Parse(Console.ReadLine());
            Console.Write("Jumlah apel = ");
            apel = int.Parse(Console.ReadLine());

            Console.WriteLine("=== Hasil Perbandingan ===");
            Console.WriteLine($"Mangga > apel : {mangga > apel}");
            Console.WriteLine($"Mangga >= apel : {mangga >= apel}");
            Console.WriteLine($"Mangga < apel : {mangga < apel}");
            Console.WriteLine($"Mangga <= apel : {mangga <= apel}");
            Console.WriteLine($"Mangga == apel : {mangga == apel}");
            Console.WriteLine($"Mangga != apel : {mangga != apel}");
        }

        static void OperatorLogika()
        {
            Console.Write("Masukkan umur anda : ");
            int age = int.Parse(Console.ReadLine());
            Console.Write("Masukkan password : ");
            string password = Console.ReadLine();

            bool isAdult = age > 18;
            bool isPasswordValid = password == "admin";

            if(isAdult && isPasswordValid)
            {
                Console.WriteLine("welcome to the club".ToUpper());
            }
            else
            {
                Console.WriteLine("Sorry, try again!");
            }
        }   
        
        static void Konversi()
        {
            int umur = 23;
            string strUmur = umur.ToString();
            Console.WriteLine(strUmur);

            int myInt = 10;
            double myDouble = 9.10;
            bool myBool = false;

            string myString = Convert.ToString(myInt); // Convert int to string
            double myConvDouble = Convert.ToDouble(myInt); // Convert int to double
            int myConvInt = Convert.ToInt32(myDouble); // Convert double to int
            string myConvString = Convert.ToString(myBool); // Convert boolean to string

            Console.WriteLine("Convert int to string : " + myString);
            Console.WriteLine("Convert int to double : " + myConvDouble);
            Console.WriteLine("Convert double to int : " + myConvInt);
            Console.WriteLine("Convert boolean to string : " + myConvString);
        }
    }
}
