﻿using System;
using System.Collections.Generic;

namespace Logic07
{
    class Mamalia
    {
        public virtual void pindah()
        {
            Console.WriteLine("Kucing Lari...");
        }
    }

    class Kucing : Mamalia
    {
    }

    class Paus : Mamalia
    {
        public override void pindah()
        {
            Console.WriteLine("Paus Berenang...");
        }
    }
    public class Program
    {
        static readonly List<Users> listofUsers = new List<Users>();
        static void Main(string[] args)
        {
            //bool ulangi = true;
            //while (ulangi)
            //{
            //    InsertNewUser();
            //}
            //ListUser();
            //DateTimes();
            //StringDateTime();
            //TimeSpan();
            //ClassInheritance();
            Overriding();

            Console.ReadKey();
        }

        static void Overriding()
        {
            Paus paus = new Paus();
            paus.pindah();

            Kucing kucing = new Kucing();
            kucing.pindah();
        }

        static void ClassInheritance()
        {
            TypeMobil typeMobil = new TypeMobil();
            typeMobil.kecepatan = 100;
            typeMobil.posisi = 50;
            typeMobil.Civic();
        }

        static void TimeSpan()
        {
            Console.WriteLine("=== Time Span ===");
            DateTime date1 = new DateTime(2023, 3, 8, 0, 0, 0);
            DateTime date2 = DateTime.Now;

            TimeSpan interval = date2 - date1;

            Console.WriteLine($"Total Hari: {interval.Days}");
            Console.WriteLine($"Total Hari: {interval.TotalDays}");
            Console.WriteLine($"Interval Jam: {interval.Hours}");
            Console.WriteLine($"Total Jam: {interval.TotalHours}");
            Console.WriteLine($"Total Menit: {interval.TotalMinutes}");
            Console.WriteLine($"Total Detik: {interval.TotalSeconds}");
        }

        static void StringDateTime()
        {
            Console.WriteLine("=== String DateTime ===");
            Console.Write("Masukkan tanggal (dd/mm/yyyy) : ");
            string strTanggal = Console.ReadLine();

            DateTime tanggal = DateTime.Parse(strTanggal);

            Console.WriteLine($"Tanggal     : {tanggal.Day}");
            Console.WriteLine($"Bulan       : {tanggal.Month}");
            Console.WriteLine($"Tahun       : {tanggal.Year}");
            Console.WriteLine($"DayOfWeek   : {(int)tanggal.DayOfWeek}");
            Console.WriteLine($"DayOfWeek   : {tanggal.DayOfWeek}");
        }

        static void DateTimes()
        {
            Console.WriteLine("=== DateTime ===");
            DateTime dt1 = new DateTime();
            Console.WriteLine(dt1);

            DateTime dt2 = DateTime.Now;
            Console.WriteLine(dt2);

            DateTime dt3 = DateTime.Now.Date;
            Console.WriteLine(dt3);

            DateTime dt4 = DateTime.UtcNow;
            Console.WriteLine(dt4);

            DateTime dt5 = new DateTime(2023, 03, 09);
            Console.WriteLine(dt5);

            DateTime dt6 = new DateTime(2023, 3, 9, 11, 45, 00);
            Console.WriteLine(dt6);
        }

        static void InsertNewUser()
        {
            Console.WriteLine("=== Insert New User ===");
            Console.Write("Input nama user baru     = ");
            string nama = Console.ReadLine();
            Console.Write("Input umur user baru     = ");
            int umur = int.Parse(Console.ReadLine());
            Console.Write("Input alamat user baru   = ");
            string alamat = Console.ReadLine();

            Console.WriteLine();

            Users user = new Users();
            user.Nama = nama;
            user.Umur = umur;
            user.Alamat = alamat;

            listofUsers.Add(user);

            int count = 0;
            foreach (var item in listofUsers)
            {
                count++;
                Console.WriteLine($"No. {count}\tNama = {item.Nama}\tUmur = {item.Umur}\tAlamat = {item.Alamat}");
            }
        }

        static void ListUser()
        {
            Console.WriteLine("=== List User ===");

            List<Users> listofUsers = new List<Users>
            {
                new Users() { Nama = "Firdha", Umur = 24, Alamat = "Tangsel" },
                new Users() { Nama = "Isni", Umur = 22, Alamat = "Cimahi" },
                new Users() { Nama = "Asti", Umur = 23, Alamat = "Garut" },
                new Users() { Nama = "Muafa", Umur = 22, Alamat = "Bogor" },
                new Users() { Nama = "Toni", Umur = 24, Alamat = "Garut" },
            };

           
            for (int i = 0; i < listofUsers.Count; i++ )
            {

                Console.WriteLine($"No. {i++}\tNama = {listofUsers[i].Nama}\tUmur = {listofUsers[i].Umur}\tAlamat = {listofUsers[i].Alamat}");
            }
        }
    }
}
