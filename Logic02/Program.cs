﻿using System;

namespace Logic02
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //IfStatement();
            //ElseStatment();
            //NestedIfStatement();
            //ImpFashion();
            //Ternary();
            //SwitchCase();
            Latihan01();
            Console.ReadKey();
        }

        static void Latihan01()
        {
            int jarijari, sisi;
            double keliling, luas;
            const double PI = 3.14;

            Console.WriteLine("=== Mencari luas dan keliling lingkaran ===");
            Console.Write("Masukkan nilai jari-jari = ");
            jarijari = int.Parse(Console.ReadLine());
            keliling = 2 * PI * jarijari;
            luas = PI * jarijari * jarijari;
            Console.WriteLine("Keliling = {0} cm", Math.Round(keliling));
            Console.WriteLine("Luas = {0} cm\xB2", Math.Round(luas));

            Console.WriteLine("\n=== Mencari luas dan keliling persegi ===");
            Console.Write("Masukkan nilai sisi persegi = ");
            sisi = int.Parse(Console.ReadLine());
            luas = sisi * sisi;
            keliling = 4 * sisi;
            Console.WriteLine("Keliling = {0} cm", Math.Round(keliling));
            Console.WriteLine("Luas = {0} cm", Math.Round(luas));
        }

        static void SwitchCase()
        {
            Console.WriteLine("=== Switch Case ===");

            Console.Write("Pilih buah kesuakaan Anda [Apel, Jeruk, Mangga, Durian] : ");
            string buah = Console.ReadLine().ToLower();

            switch (buah)
            {
                case "apel":
                    Console.WriteLine("Anda memilih buah apel");
                    break;
                case "jeruk":
                    Console.WriteLine("Anda memilih buah jeruk");
                    break;
                case "mangga":
                    Console.WriteLine("Anda memilih buah mangga");
                    break;
                case "durian":
                    Console.WriteLine("Anda memilih buah durian");
                    break;
                default:
                    Console.WriteLine("Anda memilih yang lain");
                    break;
            }
        }

        static void Ternary()
        {
            Console.WriteLine("=== Ternary ===");
            int angka;
            Console.Write("Masukkan angka = ");
            angka = int.Parse(Console.ReadLine());

            Console.WriteLine(angka % 2 == 1 ? "Bilangan ganjil" : "Bilangan genap");
        }

        static void NestedIfStatement()
        {
            Console.WriteLine("=== Nested If Statement ===");
            int umur;
          
            Console.Write("Masukkan umur : ");
            umur = int.Parse(Console.ReadLine());

            if(umur >= 20)
            {
                if(umur > 100)
                {
                    Console.WriteLine("Wah, panjang umur nih");
                } 
                else if (umur > 65)
                {
                    Console.WriteLine("Lansia");
                }
                else
                {
                    Console.WriteLine("Orang dewasa");
                }
            }
            else
            {
                if (umur < 0)
                {
                    Console.WriteLine("Kan belum lahir nih?");
                }
                else if (umur <= 5)
                {
                    Console.WriteLine("Balita");
                }
                else if (umur <= 12)
                {
                    Console.WriteLine("Anak-anak");
                }
                else
                {
                    Console.WriteLine("Remaja");
                }
            }
        }

        static void ElseStatment()
        {
            Console.WriteLine("=== Else Statement ===");            
            int a, b;

            Console.Write("Masukkan nilai a : ");
            a = int.Parse(Console.ReadLine());
            Console.Write("Masukkan nilai b : ");
            b = int.Parse(Console.ReadLine());

            if (a > b)
            {
                Console.WriteLine("Nilai a lebih besar dari nilai b");
            }
            else if(a == b)
            {
                Console.WriteLine("Nilai a sama dengan nilai b");
            }
            else
            {
                Console.WriteLine("Nilai b lebih besar dari nilai a");
            }
        }

        static void IfStatement()
        {
            Console.WriteLine("=== If Statement ===");
            int a, b;

            Console.Write("Masukkan nilai a : ");
            a = int.Parse(Console.ReadLine());
            Console.Write("Masukkan nilai b : ");
            b = int.Parse(Console.ReadLine());

            if (a > b)
            {
                Console.WriteLine("Nilai a lebih besar dari nilai b");
            }
            if (b > a)
            {
                Console.WriteLine("Nilai b lebih besar dari nilai a");
            }
            if (a == b)
            {
                Console.WriteLine("Nilai a sama dengan nilai b");
            }
        }

        static void ImpFashion()
        {
            Console.WriteLine("=== IMP Fashion ===");
            int kodeBaju, harga;
            char ukuranBaju;
            string merkBaju; 

            Console.Write("Masukkan kode baju = ");
            kodeBaju = int.Parse(Console.ReadLine());
            Console.Write("Masukkan ukuran baju = ");
            ukuranBaju = char.Parse(Console.ReadLine()); 

            if(kodeBaju == 1)
            {
                merkBaju = "IMP";
                if (ukuranBaju == 's')
                {
                    harga = 200000;
                    Console.WriteLine($"Merk baju = {merkBaju}");
                    Console.WriteLine($"Harga baju = {harga}");
                }
                else if (ukuranBaju == 'm')
                {
                    harga = 220000;
                    Console.WriteLine($"Merk baju = {merkBaju}");
                    Console.WriteLine($"Harga baju = {harga}");
                } 
                else
                {
                    harga = 250000;
                    Console.WriteLine($"Merk baju = {merkBaju}");
                    Console.WriteLine($"Harga baju = {harga}");
                }
                
            }
            else if (kodeBaju == 2)
            {
                merkBaju = "Prada";
                if (ukuranBaju == 's')
                {
                    harga = 150000;
                    Console.WriteLine($"Merk baju = {merkBaju}");
                    Console.WriteLine($"Harga baju = {harga}");
                }
                else if (ukuranBaju == 'm')
                {
                    harga = 160000;
                    Console.WriteLine($"Merk baju = {merkBaju}");
                    Console.WriteLine($"Harga baju = {harga}");
                }
                else
                {
                    harga = 170000;
                    Console.WriteLine($"Merk baju = {merkBaju}");
                    Console.WriteLine($"Harga baju = {harga}");
                }
            }
            else if (kodeBaju == 3)
            {
                merkBaju = "Gucci";
                if (ukuranBaju == 's')
                {
                    harga = 200000;
                    Console.WriteLine($"Merk baju = {merkBaju}");
                    Console.WriteLine($"Harga baju = {harga}");
                }
                else if (ukuranBaju == 'm')
                {
                    harga = 200000;
                    Console.WriteLine($"Merk baju = {merkBaju}");
                    Console.WriteLine($"Harga baju = {harga}");
                }
                else
                {
                    harga = 200000;
                    Console.WriteLine($"Merk baju = {merkBaju}");
                    Console.WriteLine($"Harga baju = {harga}");
                }
            }
            else
            {
                Console.WriteLine("Kode baju tidak valid");
            }
        }
    }
}
