﻿using System;

namespace Logic09
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //Soal01();
            //Soal02();
            //Soal03();
            //Soal03Ver2();
            Soal03Ver3();
            Console.ReadKey();
        }

        static void Soal01()
        {
            Console.WriteLine("Jarak dari toko ke customer 1        = 2KM");
            Console.WriteLine("Jarak dari customer 1 ke customer 2  = 5M");
            Console.WriteLine("Jarak dari customer 2 ke customer 3  = 1.5KM");
            Console.WriteLine("Jarak dari customer 3 ke customer 4  = 300M");

            Console.Write("\nMasukkan angka = ");
            int angka = int.Parse(Console.ReadLine());

            double[] jarak = new double[4];

            jarak[0] = 2;
            jarak[1] = 0.5;
            jarak[2] = 1.5;
            jarak[3] = 0.3;

            double jarakTempuh = 0;

            double bensin = 2.5;
            double hasil = 0;
            string tampung = "";

            for (int i = 0; i < angka; i++)
            {
                jarakTempuh += jarak[i];
                hasil = bensin / jarak[i];
            }

            if (jarak[0] == 2)
            {
                tampung += "2KM + ";
            }
            if (jarak[1] == 0.5)
            {
                tampung += "500M + ";
            }
            if (jarak[2] == 1.5)
            {
                tampung += "1.5KM =";
            }
          
            Console.WriteLine($"\nJarak tempuh = {tampung} {jarakTempuh}KM");
            Console.WriteLine($"Bensin yang dibutuhkan = {Math.Round(hasil, MidpointRounding.ToPositiveInfinity)} Liter");
        }

        static void Soal02()
        {
            double terigu = 150;
            double gulaPasir = 190;
            double susu = 100;

            Console.Write("Masukkan jumlah kue pukis = ");
            int kue = int.Parse(Console.ReadLine());

            double totalTerigu = terigu * kue / 15;
            double totalGulaPasir = gulaPasir * kue / 15;
            double totalSusu = susu * kue / 15;

            Console.WriteLine($"Total terigu yang dibutuhkan = {Math.Round(totalTerigu)}gr");
            Console.WriteLine($"Total gula pasir yang dibutuhkan = {Math.Round(totalGulaPasir)}gr");
            Console.WriteLine($"Total susu yang dibutuhkan = {Math.Round(totalSusu)}mL");
        }

        static void Soal03()
        {
            Console.Write("Masukkan banyak suku = ");
            int suku = int.Parse(Console.ReadLine());
            Console.Write("Masukkan kelipatan = ");
            int kelipatan = int.Parse(Console.ReadLine());

            int rasio = kelipatan;
            int hasil = kelipatan;

            for (int y = 1; y <= suku; y++)
            {
                for (int x = 1; x <= suku; x++)
                {
                    if(x == 1)
                    {
                        kelipatan = rasio;
                        Console.Write(kelipatan + "\t");
                    }
                    else if (y == suku)
                    {
                        kelipatan += rasio;
                        Console.Write(kelipatan + "\t");
                    }
                    else if (x == y)
                    {
                        hasil += rasio;
                        Console.Write(hasil);
                    }
                    else
                    {
                        Console.Write("\t");
                    }
                }
                Console.WriteLine("\n");
            }
        }

        static void Soal03Ver2()
        {
            Console.Write("Masukkan banyak suku = ");
            int suku = int.Parse(Console.ReadLine());
            Console.Write("Masukkan angka awal = ");
            int angka = int.Parse(Console.ReadLine());

            for (int i = 0; i < suku; i++)
            {
                int x = 0;

                for (int j = 0; j < suku; j++)
                {
                    x += angka;
                    if(j == 0 || i == suku - 1 || i == j)
                    {
                        Console.Write($"{x.ToString()}\t");
                    }
                    else
                    {
                        Console.Write("\t");
                    }
                }
                Console.WriteLine("\n");
            }
        }

        static void Soal03Ver3()
        {
            Console.Write("Masukkan banyak suku = ");
            int suku = int.Parse(Console.ReadLine());
            Console.Write("Masukkan angka awal = ");
            int angka = int.Parse(Console.ReadLine());

            for (int i = 0; i < suku; i++)
            {
                int x = 0;

                for (int j = 0; j < suku; j++)
                {
                    x += angka;
                    if (j < suku - 1 - i)
                    {
                        Console.Write("\t");
                    }
                    else
                    {
                        Console.Write($"{x.ToString()}\t");
                    }
                }
                Console.WriteLine("\n");
            }
        }
    }
}
