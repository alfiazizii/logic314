--DDL

--CREATE DATABASE--
CREATE DATABASE db_kampus

USE db_kampus

--CREATE TABLE
CREATE TABLE mahasiswa (
id bigint primary key identity(1,1),
name varchar (50) not null,
address varchar (50) not null,
email varchar (255) null
)

CREATE TABLE biodata (
id bigint primary key identity(1,1),
dob datetime not null,
kota varchar (100) not null,
)

--CREATE VIEW
CREATE VIEW vwMahasiswa
AS
SELECT id,name,address,email FROM mahasiswa

--SELECT VIEW
SELECT * FROM vwMahasiswa

--ALTER ADD COLUMN
ALTER TABLE mahasiswa ADD [description] varchar(255)
ALTER TABLE biodata ADD mahasiswa_id BIGINT FOREIGN KEY REFERENCES mahasiswa(id) NOT NULL

--ALTER COLUMN
ALTER TABLE biodata ALTER COLUMN mahasiswa_id NOT NULL

--ALTER DROP COLUMN
ALTER TABLE mahasiswa DROP COLUMN [description]

--TABLE ALTER COLUMN
ALTER TABLE biodata ALTER COLUMN dob date NOT NULL

----------------------------------------

--DROP DATABASE
DROP DATABASE db_kampus2

--DROP TABLE
DROP TABLE mahasiswa2

--DROP VIEW
DROP VIEW vwMahasiswa


--DML

--INSERT DATA
INSERT INTO mahasiswa(name, address, email) 
VALUES('Marchel','Medan','marchelajadeh@gmail.com')

INSERT INTO mahasiswa(name, address, email)
VALUES
('Toni','Garut','toni@gmail.com'),
('Isni','Cimahi','isni@gmail.com')

INSERT INTO biodata(dob, kota, mahasiswa_id)
VALUES
('2000-10-12','Jakarta',1),
('1999-05-10','Jakarta',2),
('2000-01-20','Jakarta',4)

--SELECT DATA
SELECT id,name,address,email FROM mahasiswa
SELECT * FROM mahasiswa

--UPDATE DATA
UPDATE mahasiswa SET name = 'Isni Dwitiniardi', address='Sumedang' WHERE name='isni'

UPDATE mahasiswa SET panjang = 48 WHERE id = 1
UPDATE mahasiswa SET panjang = 86 WHERE id = 2
UPDATE mahasiswa SET panjang = 117 WHERE id = 4
UPDATE mahasiswa SET panjang = 50 WHERE id = 5

--DELETE DATA
DELETE FROM mahasiswa WHERE name LIKE 'Isni%'

--JOIN
SELECT mhs.id, mhs.name, bio.kota, bio.dob, MONTH(bio.dob) as bulanlLahir, YEAR(bio.dob) as tahunLahir FROM mahasiswa AS mhs 
JOIN biodata as bio ON mhs.id = bio.mahasiswa_id WHERE mhs.name = 'toni' OR kota='Kalibata'

--ORDER BY
SELECT * 
FROM mahasiswa mhs 
JOIN biodata bio ON mhs.id = bio.mahasiswa_id 
ORDER BY mhs.id ASC, mhs.name DESC

--SELECT TOP
SELECT TOP 1 * FROM mahasiswa ORDER BY name ASC

--SELECT BETWEEN 
SELECT * FROM mahasiswa WHERE id BETWEEN 1 AND 3
SELECT * FROM mahasiswa WHERE id >= 1 AND id <= 3

--LIKE
SELECT * FROM mahasiswa WHERE name LIKE 'm%'
SELECT * FROM mahasiswa WHERE name LIKE '%i'
SELECT * FROM mahasiswa WHERE name LIKE '%ni%'
SELECT * FROM mahasiswa WHERE name LIKE '_o%'
SELECT * FROM mahasiswa WHERE name LIKE '__n%'
SELECT * FROM mahasiswa WHERE name LIKE 't_%'
SELECT * FROM mahasiswa WHERE name LIKE 't___%'
SELECT * FROM mahasiswa WHERE name LIKE 'i%i'

--GROUP BY
SELECT COUNT(id), name FROM mahasiswa GROUP BY name

--HAVING
SELECT COUNT(id), name FROM  mahasiswa GROUP BY name
HAVING COUNT(id) > 1

--DISTINCT (Menghilangkan duplikat)
SELECT DISTINCT name FROM mahasiswa

--SUBSTRING
SELECT SUBSTRING('SQL TUTORIAL', 1, 3) AS ExtractString

--CHARINDEX
SELECT CHARINDEX('t', 'Customer') AS PosisiIndex

--DATALENGTH
SELECT DATALENGTH('AKUMAU.ISTIRAHAT')

--CASE WHEN
SELECT id,name,address,panjang,
CASE WHEN panjang < 50 THEN 'Pendek'
WHEN panjang <= 100 THEN 'Sedang'
ELSE 'Tinggi'
END AS status
FROM mahasiswa

--CONCAT
SELECT CONCAT('SQL ', 'IS ', 'FUN') as Text
SELECT 'SQL ' + 'is ' + 'FUN'

--------------------------------------------

CREATE TABLE penjualan (
id bigint primary key identity(1,1),
nama varchar (50) not null,
harga int not null
)

INSERT INTO penjualan(nama, harga)
VALUES 
('Indomie',1500), ('Close-Up',3500), ('Pepsoden',3000),
('Brush Formula',2500), ('Roti Manis',1000), ('Gula',3500),
('Sarden',4500), ('Rokok Sampurna',11000), ('Rokok 234',11000)

SELECT nama, harga, harga * 100 AS [harga * 100] FROM penjualan