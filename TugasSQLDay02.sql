--TUGAS SQL DAY 02

CREATE DATABASE DB_Entertainer

CREATE TABLE artis(
kd_artis varchar (100) PRIMARY KEY NOT NULL,
nm_artis varchar (100) not null,
jk varchar (100) not null,
bayaran int not null,
award int not null,
negara varchar (100) not null
)

CREATE TABLE film(
kd_film varchar (10) PRIMARY KEY NOT NULL,
nm_film varchar (55) NOT NULL,
genre varchar (55) NOT NULL,
artis varchar (55) NOT NULL,
produser varchar (55) NOT NULL,
pendapatan int NOT NULL,
nominasi int NOT NULL
)

CREATE TABLE produser(
kd_produser varchar (50) PRIMARY KEY NOT NULL,
nm_produser varchar (50) NOT NULL,
international varchar (50) NOT NULL,
)

CREATE TABLE negara(
kd_negara varchar (100) PRIMARY KEY NOT NULL,
nm_negara varchar (100) NOT NULL,
)

CREATE TABLE genre(
kd_genre varchar (50) PRIMARY KEY NOT NULL,
nm_genre varchar (55) NOT NULL,
)

INSERT INTO artis VALUES
('A001','ROBERT DOWNEY JR','PRIA',300000000,2,'AS'),
('A002','ANGELINA JOLIE','WANITA',700000000,1,'AS'),
('A003','JACKIE CHAN','PRIA',200000000,7,'HK'),
('A004','JOE TASLIM','PRIA',350000000,1,'ID'),
('A005','CHELSEA ISLAN','WANITA',300000000,0,'ID')

INSERT INTO film VALUES
('F001','IRON MAN','G001','A001','PD01',2000000000,3),
('F002','IRON MAN 2','G001','A001','PD01',1800000000,2),
('F003','IRON MAN 3','G001','A001','PD01',1200000000,0),
('F004','AVENGER : CIVIL WAR','G001','A001','PD01',2000000000,1),
('F005','SPIDERMAN HOME COMING','G001','A001','PD01',1300000000,0),
('F006','THE RAID','G001','A004','PD03',800000000,5),
('F007','FAST & FURIOUS','G001','A004','PD05',830000000,2),
('F008','HABIBIE DAN AINUN','G004','A005','PD03',670000000,4),
('F009','POLICE STORY','G001','A003','PD02',700000000,3),
('F010','POLICE STORY 2','G001','A003','PD02',710000000,1),
('F011','POLICE STORY 3','G001','A003','PD02',615000000,0),
('F012','RUSH HOUR','G003','A003','PD05',695000000,2),
('F013','KUNGFU PANDA','G003','A003','PD05',923000000,5)

INSERT INTO produser VALUES
('PD01','MARVEL','YA'),
('PD02','HONGKONG CINEMA','YA'),
('PD03','RAPI FILM','TIDAK'),
('PD04','PARKIT','TIDAK'),
('PD05','PARAMOUNT PICTURE','YA')

INSERT INTO negara VALUES
('AS','AMERIKA SERIKAT'),
('HK','HONGKONG'),
('ID','INDONESIA'),
('IN','INDIA')

INSERT INTO genre VALUES
('G001','ACTION'),
('G002','HORROR'),
('G003','COMEDY'),
('G004','DRAMA'),
('G005','THRILLER'),
('G006','FICTION')

--1. Menampilkan jumlah pendapatan produser marvel secara keseluruhan											
SELECT p.nm_produser, SUM(CAST(f.pendapatan AS bigint)) pendapatan FROM produser p 
JOIN film f ON p.kd_produser = f.produser
WHERE p.nm_produser = 'MARVEL'
GROUP BY p.nm_produser

--2. Menampilkan nama film dan nominasi yang tidak mendapatkan nominasi											
SELECT nm_film, nominasi FROM film WHERE nominasi = 0

--3. Menampilkan nama film yang huruf depannya 'p'									
SELECT nm_film FROM film WHERE nm_film LIKE 'P%'

--4. Menampilkan nama film yang huruf terakhir 'y'								
SELECT nm_film FROM film WHERE nm_film LIKE '%y'

--5. Menampilkan nama film yang mengandung huruf 'd'									
SELECT nm_film FROM film WHERE nm_film LIKE '%d%'

--6. Menampilkan nama film dan artis						
SELECT f.nm_film, a.nm_artis 
FROM film f 
JOIN artis a ON f.artis = a.kd_artis 

--7. Menampilkan nama film yang artisnya berasal dari negara hongkong											
SELECT f.nm_film, a.negara 
FROM film f 
JOIN artis a ON f.artis = a.kd_artis
WHERE a.negara = 'HK'

--8. Menampilkan nama film yang artisnya bukan berasal dari negara yang mengandung huruf 'o'											
SELECT f.nm_film, n.nm_negara 
FROM film f 
JOIN artis a ON f.artis = a.kd_artis
JOIN negara n ON a.negara = n.kd_negara 
WHERE n.nm_negara NOT LIKE '%O%'

--9. Menampilkan nama artis yang tidak pernah bermain film										
SELECT a.nm_artis 
FROM artis a 
LEFT JOIN film f ON a.kd_artis = f.artis
WHERE f.kd_film IS NULL

--10. Menampilkan nama artis yang bermain film dengan genre drama											
SELECT a.nm_artis, g.nm_genre 
FROM artis a
JOIN film f ON a.kd_artis = f.artis
JOIN genre g ON g.kd_genre = f.genre 
WHERE g.nm_genre = 'DRAMA'

--11. Menampilkan nama artis yang bermain film dengan genre Action											
SELECT DISTINCT a.nm_artis, g.nm_genre 
FROM artis a
JOIN film f ON a.kd_artis = f.artis
JOIN genre g ON g.kd_genre = f.genre 
WHERE g.nm_genre = 'ACTION'

--12. Menampilkan data negara dengan jumlah filmnya									
SELECT n.kd_negara, n.nm_negara, COUNT(f.kd_film) jumlah_Film 
FROM negara n 
LEFT JOIN artis a ON n.kd_negara = a.negara 
LEFT JOIN film f ON f.artis = a.kd_artis
GROUP BY n.kd_negara, nm_negara
ORDER BY n.nm_negara ASC

--13. Menampilkan nama film yang skala internasional									
SELECT f.nm_film FROM film f 
JOIN produser p ON f.produser = p.kd_produser
WHERE p.international = 'YA'

SELECT f.nm_film, p.nm_produser FROM film f 
JOIN produser p ON f.produser = p.kd_produser
WHERE p.nm_produser NOT LIKE '%A'

--14. Menampilkan jumlah film dari masing2 produser									
SELECT p.nm_produser, COUNT(f.kd_film) jumlah_film FROM produser p 
LEFT JOIN film f ON p.kd_produser = f.produser
GROUP BY p.nm_produser
