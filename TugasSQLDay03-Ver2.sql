CREATE DATABASE DB_HR

CREATE TABLE tb_karyawan(
id bigint primary key identity(1,1),
nip varchar(50) not null,
nama_depan varchar(50) not null,
nama_belakang varchar(50) not null,
jenis_kelamin varchar(50) not null,
agama varchar(50) not null,
tempat_lahir varchar(50) not null,
tgl_lahir date not null,
alamat varchar(100) not null,
pendidikan_terakhir varchar(50) not null,
tgl_masuk date not null
)

INSERT INTO tb_karyawan(nip,nama_depan,nama_belakang,jenis_kelamin,agama,tempat_lahir,tgl_lahir,alamat,pendidikan_terakhir,tgl_masuk)
VALUES
('001','Hamidi','Samsudin','Pria','Islam','Sukabumi','1977-04-21','Jl. Sudirman No.12','S1 Teknik Mesin','2015-12-07'),
('002','Ghandi','Wamida','Wanita','Islam','Palu','1992-01-12','Jl. Rambutan No.22','SMA Negeri 02 Palu','2014-12-01'),
('003','Paul','Christian','Pria','Kristen','Ambon','1980-05-27','Jl. Veteran No.4','S1 Pendidikan Geografi','2014-01-12')

SELECT * FROM tb_karyawan

CREATE TABLE tb_divisi(
id bigint primary key identity(1,1),
kd_divisi varchar(50) not null,
nama_divisi varchar(50) not null
)

INSERT INTO tb_divisi(kd_divisi,nama_divisi)
VALUES
('GD','Gudang'),
('HRD','HRD'),
('KU','Keuangan'),
('UM','Umum')

CREATE TABLE tb_jabatan(
id bigint primary key identity(1,1),
kd_jabatan varchar(50) not null,
nama_jabatan varchar(50) not null,
gaji_pokok numeric,
tunjangan_jabatan numeric
)

INSERT INTO tb_jabatan(kd_jabatan,nama_jabatan,gaji_pokok,tunjangan_jabatan)
VALUES
('MGR','Manager',5500000,1500000),
('OB','Office Boy',1900000,200000),
('ST','Staff',3000000,750000),
('WMGR','Wakil Manager',4000000,1200000)

CREATE TABLE tb_pekerjaan(
id bigint primary key identity(1,1),
nip varchar(50) not null,
kode_jabatan varchar(50) not null,
kode_divisi varchar(50) not null,
tunjangan_kinerja numeric,
kota_penempatan varchar(50)
)

INSERT INTO tb_pekerjaan(nip,kode_jabatan,kode_divisi,tunjangan_kerja,kota_penempatan)
VALUES
('001','ST','KU',750000,'Cianjur'),
('002','OB','UM',350000,'Sukabumi'),
('003','MGR','HRD',1500000,'Sukabumi')

--1. Tampilkan nama lengkap, nama jabatan, tunjangan jabatan + gaji , yang gaji + tunjangan kinerja dibawah 5juta
SELECT CONCAT(tbk.nama_depan, ' ', tbk.nama_belakang) [nama_lengkap], tbj.nama_jabatan, (tbj.tunjangan_jabatan + tbj.gaji_pokok) [gaji_tunjangan]
FROM tb_karyawan tbk
JOIN tb_pekerjaan tbp ON tbk.nip = tbp.nip
JOIN tb_jabatan tbj ON tbj.kd_jabatan = tbp.kode_jabatan
WHERE (tbj.gaji_pokok + tbp.tunjangan_kinerja) < 5000000

--2. Tampilkan nama lengkap, jabatan, nama divisi, total gaji, pajak, gaji bersih, yg gendernya pria dan penempatan kerjanya diluar sukabumi
SELECT CONCAT(tbk.nama_depan, ' ', tbk.nama_belakang) [nama_lengkap], tbj.nama_jabatan, tbd.nama_divisi, (tbj.gaji_pokok + tbj.tunjangan_jabatan + tbp.tunjangan_kinerja) [total_gaji], (tbj.gaji_pokok + tbj.tunjangan_jabatan + tbp.tunjangan_kinerja) * 0.05[pajak],(tbj.gaji_pokok + tbj.tunjangan_jabatan + tbp.tunjangan_kinerja) - (tbj.gaji_pokok + tbj.tunjangan_jabatan + tbp.tunjangan_kinerja) * 0.05 [gaji_bersih]
FROM tb_karyawan tbk
JOIN tb_pekerjaan tbp ON tbk.nip = tbp.nip
JOIN tb_jabatan tbj ON tbj.kd_jabatan = tbp.kode_jabatan
JOIN tb_divisi tbd ON tbd.kd_divisi = tbp.kode_divisi
WHERE tbp.kota_penempatan NOT LIKE 'Sukabumi'

--3. Tampilkan nip, nama lengkap, jabatan, nama divisi, bonus (bonus=25% dari total gaji(gaji pokok+tunjangan_jabatan+tunajangan_kinerja) * 7
SELECT tbk.nip, CONCAT(tbk.nama_depan, ' ', tbk.nama_belakang) [nama_lengkap], tbj.nama_jabatan, tbd.nama_divisi, (tbj.gaji_pokok + tbj.tunjangan_jabatan + tbp.tunjangan_kinerja) * 7 * 0.25 [bonus]
FROM tb_karyawan tbk
JOIN tb_pekerjaan tbp ON tbk.nip = tbp.nip
JOIN tb_jabatan tbj ON tbj.kd_jabatan = tbp.kode_jabatan
JOIN tb_divisi tbd ON tbd.kd_divisi = tbp.kode_divisi

--4. Tampilkan nama lengkap, ttal gaji, infak(5%*total gaji) yang mempunyai jabatan MGR								
SELECT tbk.nip, CONCAT(tbk.nama_depan, ' ', tbk.nama_belakang) [nama_lengkap], tbj.nama_jabatan, tbd.nama_divisi, (tbj.gaji_pokok + tbj.tunjangan_jabatan + tbp.tunjangan_kinerja)[total_gaji],(tbj.gaji_pokok + tbj.tunjangan_jabatan + tbp.tunjangan_kinerja) * 0.05[infak] 
FROM tb_karyawan tbk
JOIN tb_pekerjaan tbp ON tbk.nip = tbp.nip
JOIN tb_jabatan tbj ON tbj.kd_jabatan = tbp.kode_jabatan
JOIN tb_divisi tbd ON tbd.kd_divisi = tbp.kode_divisi
WHERE tbj.kd_jabatan = 'MGR'

--5. Tampilkan nama lengkap, nama jabatan, pendidikan terakhir, tunjangan pendidikan(2jt), dan total gaji(gapok+tjabatan+tpendidikan) dimana pendidikan akhirnya adalah S1	
SELECT tbk.nip, CONCAT(tbk.nama_depan, ' ', tbk.nama_belakang) [nama_lengkap], tbj.nama_jabatan, tbk.pendidikan_terakhir, 2000000 [tunjangan_pendidikan],(tbj.gaji_pokok + tbj.tunjangan_jabatan + 2000000)[total_gaji]
FROM tb_karyawan tbk
JOIN tb_pekerjaan tbp ON tbk.nip = tbp.nip
JOIN tb_jabatan tbj ON tbj.kd_jabatan = tbp.kode_jabatan
JOIN tb_divisi tbd ON tbd.kd_divisi = tbp.kode_divisi
WHERE tbk.pendidikan_terakhir LIKE 'S1%'
ORDER BY tbp.nip

--6. Tampilkan nip, nama lengkap, jabatan, nama divisi, bonus					
SELECT tbk.nip, CONCAT(tbk.nama_depan, ' ', tbk.nama_belakang) [nama_lengkap], tbj.nama_jabatan, tbd.nama_divisi,CASE WHEN tbj.kd_jabatan = 'MGR' THEN (tbj.gaji_pokok + tbj.tunjangan_jabatan + tbp.tunjangan_kinerja) * 7 * 0.25 WHEN tbj.kd_jabatan = 'ST' THEN (tbj.gaji_pokok + tbj.tunjangan_jabatan + tbp.tunjangan_kinerja) * 5 * 0.25 ELSE (tbj.gaji_pokok + tbj.tunjangan_jabatan + tbp.tunjangan_kinerja) * 2 * 0.25 END AS bonus
FROM tb_karyawan tbk
JOIN tb_pekerjaan tbp ON tbk.nip = tbp.nip
JOIN tb_jabatan tbj ON tbj.kd_jabatan = tbp.kode_jabatan
JOIN tb_divisi tbd ON tbd.kd_divisi = tbp.kode_divisi
ORDER BY tbk.nip

--7. Buatlah kolom nip pada table karyawan sebagai kolom unique					
ALTER TABLE tb_karyawan
ADD CONSTRAINT unique_nip UNIQUE(nip)

--8. Buatlah kolom nip pada table karyawan sebagai index					
CREATE INDEX index_nip
ON tb_karyawan(nip)

--9. Tampilkan nama lengkap, nama belakangnya diubah menjadi huruf capital dengan kondisi nama belakang di awali dengan huruf W				
SELECT CONCAT(nama_depan, ' ', nama_belakang) [nama_lengkap],  UPPER(nama_belakang) [nama_belakang]
FROM tb_karyawan
WHERE nama_belakang LIKE 'W%'

--10.  Perusahaan akan memberikan bonus sebanyak 10% dari total gaji bagi karyawan yg sudah join di peruashaan diatas sama dengan  8 tahun
SELECT CONCAT(tbk.nama_depan, ' ', tbk.nama_belakang) [nama_lengkap], tbk.tgl_masuk, tbj.nama_jabatan, tbd.nama_divisi, (tbj.gaji_pokok + tbj.tunjangan_jabatan + tbp.tunjangan_kinerja) [total_gaji], DATEDIFF(YEAR,tbk.tgl_masuk,GETDATE()) [lama_bekerja]
FROM tb_karyawan tbk
JOIN tb_pekerjaan tbp ON tbk.nip = tbp.nip
JOIN tb_jabatan tbj ON tbj.kd_jabatan = tbp.kode_jabatan
JOIN tb_divisi tbd ON tbd.kd_divisi = tbp.kode_divisi
GROUP BY tbk.nama_depan, tbk.nama_belakang, tbk.tgl_masuk, tbj.nama_jabatan, tbd.nama_divisi, tbj.gaji_pokok, tbj.tunjangan_jabatan, tbp.tunjangan_kinerja
HAVING DATEDIFF(YEAR,tbk.tgl_masuk,GETDATE()) >= 8
